# CustomGrid

# Kendo UI Grid

## Description

Kendo UI grid that fetches data from remote DB. There is multiple selection with checkboxes and upon selection Edit and Remove button appear. Once you have taken actions on any item the selection is removed. We can add new items to the grid, remove items, edit items. We can discard adding new item, or can cancel editing one. There are custom page sizes - 10, 25 or 100. All changes done to the table will take place in the DB only when Save Changes button is hit.

---

##  Project Requirements

### Server

1. Go to project/api folder then in terminal and run:

 ```sh
  $ npm install
  ```

2. Setup MySQL Database with new Schema.

3. Setup `оrmconfig.json` file. It needs to be on root level in api folder where is `package.json` and other config files.

- `ormconfig.json` file with your settings:

``` sh
{
  "type": "mariadb",
  "host": "localhost",
  "port": 3306,
  "username": "root",
  "password": "root",
  "database": "demodatadb",
  "synchronize": true,
  "logging": false,
  "entities": [
    "src/models/**/*.entity.ts"
  ],
  
  "cli": {
    "entitiesDir": "src/models",
    "migrationsDir": "src/data/migration"
  }
}
```

4. Generate seed data:
 
 ```sh
  $ npm run seed
  ```
   
5. After files are setup open the terminal and run the following command:

 ```sh
  $ npm run start:dev
  ```

### Client

6. Navigate to the `client` folder. Open the terminal and run the following commands:

  ```sh
  $ npm install
  ```

  ```sh
  $ npm run start
  ```
  
---

### Built With

 - [React JS](https://reactjs.org/) - library used for our client.
 - [NestJS](https://nestjs.com/) - framework for building our server.
 - [Kendo UI](https://www.telerik.com/kendo-ui) - to design our components in the client.
 - [TypeORM](http://typeorm.io/#/) - an **ORM** that can run in **Node.js**;

---

### Authors and Contributors

- [Silviya Naumova](https://gitlab.com/snaumova)

---