import React, { Component } from 'react';
import '@progress/kendo-theme-default/dist/all.css';
import './App.css';
import GridTable from './components/gridTable';

class App extends Component {
  
  render() {
    return (
        <GridTable /> 
    );
  }
}


export default App;