import * as React from 'react';
import { Grid, GridColumn as Column, GridColumn } from '@progress/kendo-react-grid';
import { MyCommandCell } from "../components/myCommandCell.jsx";
import { Button } from '@progress/kendo-react-buttons';

export default function GridTable(props) {
    const editField = "inEdit";

    // setting the state and the initial state
    const [state, setState] = React.useState({
        items: [],
        total: 0,
        skip: 0,
        pageSize: 10,
        pageable: {
            buttonCount: 5,
            info: true,
            type: 'numeric',
            pageSizes: [10, 25, 100],
            previousNext: true
        },
        data: []
    });

    const [data, setData] = React.useState(state.data);

    // fetching the initial data from the DB
    React.useEffect(() => {
        fetch('http://localhost:3000/data')
            .then(response => response.json())
            .then(results => {
                if (results.error) {
                    // alert(`${results.error}`)
                } else {
                    const data = results.filter((result) => !result.isDeleted)
                    setState(createState(0, 10, data.map(i => ({ ...i, productName: i.productName, productID: i.id, unitPrice: i.unitPrice, selected: false, inEdit: false })))
                    );
                    setData(results)
                }

            })
    }, [data.name, data.unitPrice]);

    // updating the state upon page change
    const pageChange = (event) => {
        setState({
            items: state.data.slice(event.page.skip, event.page.skip + event.page.take),
            skip: event.page.skip,
            take: event.page.take,
            total: state.data.length,
            pageSize: event.page.take,
            pageable: {
                buttonCount: 5,
                info: true,
                type: 'numeric',
                pageSizes: [10, 25, 100],
                previousNext: true
            },
            data: state.data.map(dataItem => Object.assign({ selected: false }, dataItem))
        });
    }

    // creating the state when new fetch requests are being done
    const createState = (skip, take, data) => {
        return {
            items: data.slice(skip, skip + take),
            total: data.length,
            skip: skip,
            pageSize: take,
            pageable: {
                buttonCount: 5,
                info: true,
                type: 'numeric',
                pageSizes: [10, 25, 100],
                previousNext: true
            },
            data: data.map(dataItem => Object.assign({ selected: false }, dataItem))
        };
    }

    // setting up selection and multiple selection
    const selectionChange = (event) => {
        const data = state.data.map(item => {
            if (item.productID === event.dataItem.productID) {
                item.selected = !event.dataItem.selected;
            }
            return item;
        });
        setState(createState(state.skip, state.pageSize, data));
    }

    const headerSelectionChange = (event) => {
        const checked = event.syntheticEvent.target.checked;
        const data = state.data.map(item => {
            item.selected = checked;
            return item;
        });
        setState(createState(state.skip, state.pageSize, data));
    }

    // all buttons modifying the data items are in a separate file and combined and rendered here at once
    const CommandCell = (props, match) => {
        return (
            <MyCommandCell
                {...props}
                edit={enterEdit}
                remove={remove}
                add={add}
                discard={discard}
                update={update}
                cancel={cancel}
                editField={editField}
            />
        );
    }

    // removing an item
    const remove = (dataItem) => {
        const data = deleteItem(dataItem);
        setState(createState(state.skip, state.pageSize, data));
    };

    // adding a new item - checking the number of items on the DB and setting the productID based on the length
    const add = async (dataItem) => {
        let numberOfItemsToBeAdded = 0;
        const dataInDB = await fetch('http://localhost:3000/data')
            .then(response => response.json())
            .then(results => {
                if (results.error) {
                    // alert(`${results.error}`)
                } else {
                    return results.map((result) => result.id);
                }

            })
        if (dataInDB) {
            numberOfItemsToBeAdded = state.data.filter((item) => dataInDB.indexOf(item.productID) === -1).length;
            dataItem.productID = dataInDB.length + numberOfItemsToBeAdded;

        } else {
            numberOfItemsToBeAdded = state.data.length;
            dataItem.productID = numberOfItemsToBeAdded;
        }
        dataItem.inEdit = false;

        insertItem(dataItem);
    };

    // adding the new item to the state data
    const insertItem = item => {
        item.inEdit = false;
        item.selected = false;
        state.data.shift();
        data[state.total - 1] = item;
        setState(createState(state.skip, state.pageSize, [item, ...state.data]))
    };

    // updating an item and updating the state data
    const update = (dataItem) => {
        dataItem.inEdit = false;
        dataItem.selected = false;
        updateItem(dataItem);
        setState(createState(state.skip, state.pageSize, [...state.data]));
    };
    const updateItem = item => {
        let index = state.data.findIndex(record => record.productID === item.productID);

        state.data[index] = item;
        data[index] = item;
        return state.data;
    };

    // discarding the adding of new item
    const discard = dataItem => {
        const data = [...state.data];
        data.splice(0, 1)
        setState(createState(state.skip, state.pageSize, data));
    };

    // canceling the editing of the item to the original item state
    const cancel = dataItem => {
        const originalItem = data.find((product) => product.id === dataItem.productID || product.productID === dataItem.productID);
        const datas = state.data.map(item =>
            item.productID === originalItem.id || item.productID === originalItem.productID ? { ...item, inEdit: false, selected: false, productName: originalItem.productName, unitPrice: +originalItem.unitPrice } : item
        );
        setState(createState(state.skip, state.pageSize, datas));
    };

    // entering editing state
    const enterEdit = dataItem => {
        setState(createState(state.skip, state.pageSize, state.data.map(item =>
            item.productID === dataItem.productID || item.selected === true ? { ...item, inEdit: true, unitPrice: +item.unitPrice } : item)));
    };

    // updating item state upon any change
    const itemChange = event => {
        const data = state.data.map(item =>
            item.productID === event.dataItem.productID
                ? { ...item, [event.field]: event.value }
                : item
        );

        setState(createState(state.skip, state.pageSize, data));
    };

    // adding new row for entering new item
    const addNew = () => {
        const newDataItem = { inEdit: true, selected: false, productID: undefined };
        setState(createState(state.skip, state.pageSize, [newDataItem, ...state.data]));
    };

    // deleting an item and updating the state
    const deleteItem = item => {
        let index = state.data.findIndex(record => record.productID === item.productID);
        state.data.splice(index, 1);
        return state.data;
    };

    // upon saving changes everything done on the grid as addition, removal or updating any item that is not yet counted on the DB, will be executed all at ones upon using the appropriate checks
    const saveChanges = async (items) => {

        const dataInDB = await fetch('http://localhost:3000/data')
            .then(response => response.json())
            .then(results => {
                if (results.error) {
                    // alert(`${results.error}`)
                } else {
                    const dataInDB = results.map(i => ({ ...i, productName: i.productName, productID: i.id, unitPrice: +i.unitPrice, selected: false, inEdit: false, isDeleted: i.isDeleted }))
                    return dataInDB;
                }

            })
        if (dataInDB) {
            const itemsIDs = items.map((item) => item.productID);
            const itemsDBIDs = dataInDB.map((item) => item.id);

            // filter items that needs to be added to the DB
            const itemsToAddIDs = itemsIDs.filter((ID) => itemsDBIDs.indexOf(ID) === -1);
            const addToDBItems = items.filter((item) => itemsToAddIDs.indexOf(item.productID) > -1);

            // filter items that need to be removed from the DB
            const itemsToRemoveDB = dataInDB.filter((item) => itemsIDs.indexOf(item.id) === -1 && item.isDeleted !== true)

            // filter items that need to be updated on the DB
            const itemsToUpdateInDB = items.filter((item) => dataInDB[item.productID - 1] && !dataInDB[item.productID - 1].isDeleted && (item.productName !== dataInDB[item.productID - 1].productName || item.unitPrice !== dataInDB[item.productID - 1].unitPrice));

            // if any items are found to be added fetch request will be send to the server
            if (addToDBItems.length >= 1) {
                await addToDBItems.forEach((item) => {
                    fetch('http://localhost:3000/data', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            productName: item.productName,
                            unitPrice: item.unitPrice,
                        }),
                    })
                        .then(response => response.json())
                        .then(results => {
                            if (results.error) {
                                alert(results.error)
                            }
                        })
                })
            }

            // if any items are found to be removed fetch request will be send to the server
            if (itemsToRemoveDB.length >= 1) {
                await itemsToRemoveDB.forEach((item) => {
                    fetch(`http://localhost:3000/data/${item.productID}/delete`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                        .then(response => response.json())
                        .then(results => {
                            if (results.error) {
                                alert(results.error)
                            }
                        })
                })
            }

            // if any items are found to be updated fetch request will be send to the server
            if (itemsToUpdateInDB.length >= 1) {
                console.log(itemsToRemoveDB)
                await itemsToUpdateInDB.forEach((item) => {
                    fetch('http://localhost:3000/data', {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            id: item.productID,
                            productName: item.productName,
                            unitPrice: item.unitPrice,
                        }),
                    })
                        .then(response => response.json())
                        .then(results => {
                            if (results.error) {
                                alert(results.error)
                            }
                        })
                })
            }
            if (addToDBItems.length === 0 && itemsToRemoveDB.length === 0 && itemsToUpdateInDB.length === 0) {
                alert('No Changes to save');
            } else {
                alert('Changes saved');
            }


        } else {
            await items.forEach((item) => {
                fetch('http://localhost:3000/data', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        // ...user,
                        productName: item.productName,
                        unitPrice: item.unitPrice,
                    }),

                })
                    .then(response => response.json())
                    .then(results => {
                        if (results.error) {
                            alert(results.error)
                        }
                    })
            })
            alert('Items added')
        }




    }

    return (
        <>
            <div className="example-config row">
                <h1>Hello KendoReact!</h1>
            </div>
            <Grid
                style={{ marginBottom: '25px'}}
                data={state.items}
                onPageChange={pageChange}
                total={state.total}
                editField="inEdit"
                {...props}
                {...state.data}
                skip={state.skip}
                onItemChange={itemChange}
                pageable={state.pageable}
                pageSize={state.pageSize}
                selectedField="selected"
                onSelectionChange={selectionChange}
                onHeaderSelectionChange={headerSelectionChange}
            >

                <GridColumn
                    field="selected"
                    /*width="40%"*/
                    headerSelectionValue={
                        state.data
                            ? state.data.findIndex(dataItem => dataItem.selected === false) === -1
                            : -1
                    }
                />
                <Column /*width="120em"*/ field="productID" editable={false} />
                <Column /*width="120em"*/ field="productName" title="Product Name" editor="text" editable={true} />
                <Column /*width="120em"*/ field="unitPrice" format="{0:0.00}" title="Unit Price" editor="numeric" editable={true} />
                <Column cell={CommandCell} data={state.data} width="100px" />
            </Grid>
            <Button
                primary={true}
                title="Add new"
                className="myButton"
                onClick={() => addNew()}
            >
                Add new
                    </Button>
            <Button
                primary={true}
                title="Save Changes"
                className="myButton"
                onClick={() => saveChanges(state.data)}
            >
                Save Changes
                    </Button>
        </>
    );

}

