export class ReturnDataDTO {
    id: number;
    productName: string;
    unitPrice: number;
}