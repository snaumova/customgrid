export class UpdateDataDTO {
    id: number;
    productName: string;
    unitPrice: number;
}