import { PrimaryGeneratedColumn, Column, Entity } from "typeorm";

@Entity()
export class DemoData {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({type: 'nvarchar'})
    productName: string;

    @Column('decimal', { precision: 5, scale: 2, default: 0 })
    unitPrice: number;
    @Column({type: 'boolean', default: false})
    isDeleted: boolean;
}
