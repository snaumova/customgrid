import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { AppService } from './app.service';
import { CreateDataDTO } from './DTOs/create-data.dto';
import { UpdateDataDTO } from './DTOs/update-data.dto';
import { DemoData } from './models/demo-data.entity';

@Controller('data')
export class AppController {
    public constructor(private readonly appService: AppService) { }

    @Get()
    async getData(): Promise<DemoData[]> {

        return await this.appService.getData();
    }

    @Post()
    async createData(@Body() data: CreateDataDTO): Promise<DemoData> {

        return await this.appService.createData(data);
    }

    @Put()
    async updateData(@Body() data: UpdateDataDTO): Promise<DemoData> {

        return await this.appService.updateData(data);
    }

    @Delete(':id/delete')
    async removeData(@Param('id') dataID: string): Promise<DemoData> {
        return await this.appService.deleteData(+dataID);
    }


}
