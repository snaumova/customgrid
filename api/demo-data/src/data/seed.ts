import { DemoData } from '../models/demo-data.entity';
import { createConnection, Repository, Connection } from 'typeorm';

// SENSITIVE DATA ALERT! - Normally the seed and the admin credentials should not be present in the public repository!
// Run: `npm run seed` to seed the database

const seedProducts = async (connection: Connection) => {
  const dataRepo: Repository<DemoData> = connection.manager.getRepository(DemoData);
  const products = [{
    productName: 'Chorizo',
    unitPrice: 7.50,
  },
  {
    productName: 'Apples',
    unitPrice: 1.20,
  },
  {
    productName: 'Bananas',
    unitPrice: 2.20,
  },
  {
    productName: 'Oranges',
    unitPrice: 3.30,
  },
  {
    productName: 'Berries',
    unitPrice: 5.44,
  },
  {
    productName: 'Melon',
    unitPrice: 2.25,
  },
  {
    productName: 'Lettuce',
    unitPrice: 0.79,
  },
  {
    productName: 'Onion',
    unitPrice: 0.50,
  },
  {
    productName: 'Garlic',
    unitPrice: 1.10,
  },
  {
    productName: 'Beans',
    unitPrice: 2.00,
  },
  {
    productName: 'Ice cream',
    unitPrice: 2.25,
  },
  {
    productName: 'Soda',
    unitPrice: 1.25,
  },
  {
    productName: 'Pasta',
    unitPrice: 2.78,
  },
  {
    productName: 'Rice',
    unitPrice: 2.50,
  },
  {
    productName: 'Cheese',
    unitPrice: 9.99,
  },
  {
    productName: 'Snack bar',
    unitPrice: 2.50,
  },
  {
    productName: 'Cereal',
    unitPrice: 4.40,
  },
  {
    productName: 'Tortilla',
    unitPrice: 2.30,
  },
  {
    productName: 'Coffee',
    unitPrice: 1.60,
  },
  {
    productName: 'Oatmeal',
    unitPrice: 3.00,
  },
  {
    productName: 'Bread',
    unitPrice: 1.60,
  },
  {
    productName: 'Pepper',
    unitPrice: 2.50,
  },
  {
    productName: 'Jelly',
    unitPrice: 3.60,
  },
  {
    productName: 'Mustard',
    unitPrice: 4.60,
  },
  {
    productName: 'Ketchup',
    unitPrice: 3.50,
  },
  {
    productName: 'Olive oil',
    unitPrice: 15.30,
  },
  {
    productName: 'Vinegar',
    unitPrice: 0.90,
  },
  {
    productName: 'Salad dressing',
    unitPrice: 7.50,
  },
  {
    productName: 'Cucumber',
    unitPrice: 2.30,
  },
  {
    productName: 'Tomato',
    unitPrice: 3.55,
  },
  {
    productName: 'BBQ sauce',
    unitPrice: 3.50,
  },
  {
    productName: 'Dish Soap',
    unitPrice: 4.60,
  },
  {
    productName: 'Napkins',
    unitPrice: 2.00,
  },
  {
    productName: 'Plastic bags',
    unitPrice: 2.50,
  },
  {
    productName: 'Shaving cream',
    unitPrice: 8.50,
  },
  {
    productName: 'Mouthwash',
    unitPrice: 10.00,
  },
  {
    productName: 'Toothpaste',
    unitPrice: 5.50,
  },
  {
    productName: 'Body wash',
    unitPrice: 10.50,
  },
  {
    productName: 'Sponges',
    unitPrice: 1.10,
  },
  {
    productName: 'Hand soap',
    unitPrice: 4.00,
  },
  {
    productName: 'Laundry detergent',
    unitPrice: 20.00,
  },
  {
    productName: 'Batteries',
    unitPrice: 10.00,
  },
  {
    productName: 'Pizza',
    unitPrice: 5.90,
  },
  {
    productName: 'Fish',
    unitPrice: 9.90,
  },
  {
    productName: 'Chicken',
    unitPrice: 4.60,
  },
  {
    productName: 'Pork',
    unitPrice: 8.90,
  },
  ];

  for (const product of products) {
    const foundProduct = await dataRepo.findOne({
      productName: product.productName,
    });

    if (!foundProduct) {
      const userToCreate = dataRepo.create(product);
      console.log(await dataRepo.save(userToCreate));
    }
  }

};

// const beginSetup = async (connection: Connection) => {
//   const dbsetupRepo = connection.manager.getRepository(DbSetup);
//   const setupData = await dbsetupRepo.find({});

//   if (setupData.length) {
//     throw new Error(`Data has been already set up`);
//   }
// };

// const completeSetup = async (connection: Connection) => {
//   const dbsetupRepo = connection.manager.getRepository(DbSetup);

//   await dbsetupRepo.save({ message: 'Setup has been completed!' });
// };

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  try {
    // await beginSetup(connection);

    await seedProducts(connection);
    // await completeSetup(connection);
  } catch (e) {
    console.log(e.message)
  }

  console.log('Seed completed!');
  connection.close();
};

seed().catch(console.error);
