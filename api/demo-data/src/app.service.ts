import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDataDTO } from './DTOs/create-data.dto';
import { UpdateDataDTO } from './DTOs/update-data.dto';
import { DemoData } from './models/demo-data.entity';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(DemoData) private readonly demoDataRepository: Repository<DemoData>,

) { }

// gets all items from the DB
async getData(): Promise<DemoData[]> {
  const demoData = await this.demoDataRepository.find();
  if (!demoData || demoData.length === 0) {
      throw new NotFoundException('No data is found')
  }

  return demoData;
}

// creates a new Item and saves to the DB
async createData(dataDTO: CreateDataDTO): Promise<DemoData> {

  const newData = this.demoDataRepository.create(dataDTO);
  
  return await this.demoDataRepository.save(newData);
}

// updates already existing item on the DB
async updateData(dataDTO: UpdateDataDTO): Promise<DemoData> {
  const foundData = await this.demoDataRepository.findOne({ where: { id: dataDTO.id } });
  if (!foundData) {
    throw new NotFoundException('no such data is found')
  }
  foundData.productName = dataDTO.productName;
  foundData.unitPrice = dataDTO.unitPrice;
  await this.demoDataRepository.update(foundData.id, dataDTO)
  return await this.demoDataRepository.save(foundData);
}

// removes an item based on item id
async deleteData(dataID: number): Promise<DemoData> {
  const foundData = await this.demoDataRepository.findOne({ where: { id: dataID } });
  if (!foundData) {
    throw new NotFoundException('no such data is found')
  }
  foundData.isDeleted = true;
  await this.demoDataRepository.update(foundData.id, foundData)
  return await this.demoDataRepository.save(foundData); 
}


}
